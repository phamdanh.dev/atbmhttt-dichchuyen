package Lab1;

public class DichChuyen {

	// Khong dung
//	public static char[] bangChuCai = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
//			'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'W', 'Z' };

	public static String bangChuCaiStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public int giaiKhoa(int key) {
		int giaiKhoa = 0;
		giaiKhoa = 100 % key;

		return giaiKhoa;

	}

	public String dichChuyen(String in, int key) {

		String maHoa = "";

		int keyNum = giaiKhoa(key);

		for (int i = 0; i < in.length(); i++) {

			for (int j = 0; j < bangChuCaiStr.length(); j++) {

				Character x = in.charAt(i);

				Character y = bangChuCaiStr.charAt(j);

				if (x.equals(y)) {
//					maHoa += String.valueOf(bangChuCaiStr.charAt(j + keyNum));
					if (j + keyNum > 25) {
						maHoa += String.valueOf(bangChuCaiStr.charAt((j + keyNum) - 26));
					} else {
						maHoa += String.valueOf(bangChuCaiStr.charAt(j + keyNum));
					}
				}
			}

		}

		return maHoa;

	}

	public String giaiMaDichChuyen(String in, int key) {

		String giaiMa = "";

		int keyNum = giaiKhoa(key);

		for (int i = 0; i < in.length(); i++) {

			for (int j = 0; j < bangChuCaiStr.length(); j++) {

				Character x = in.charAt(i);

				Character y = bangChuCaiStr.charAt(j);

				if (x.equals(y)) {
//					giaiMa += String.valueOf(bangChuCaiStr.charAt(j - keyNum));

					if (j - keyNum < 0) {
						giaiMa += String.valueOf(bangChuCaiStr.charAt(j - keyNum + 26));
					} else {
						giaiMa += String.valueOf(bangChuCaiStr.charAt(j - keyNum));
					}
				}
			}

		}

		return giaiMa;
	}

	public static void main(String[] args) {
		String text = "XYZ";
		int key = 8;

		System.out.println("Key: " + key);

		System.out.println("Chuoi goc: " + text);

		DichChuyen dichChuyen = new DichChuyen();

		String chuoiMaHoa = dichChuyen.dichChuyen(text, key);

		System.out.println("So du: " + dichChuyen.giaiKhoa(key));

		System.out.println("Chuoi da ma hoa: " + chuoiMaHoa);

		String chuoiGiaiMa = dichChuyen.giaiMaDichChuyen(chuoiMaHoa, key);

		System.out.println("Chuoi giai ma: " + chuoiGiaiMa);
	}

}
